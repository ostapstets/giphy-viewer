package com.giphyviewer.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.giphyviewer.R
import com.giphyviewer.databinding.ItemGifBinding
import com.giphyviewer.model.GiphyResponse

class GifListAdapter : RecyclerView.Adapter<GifListAdapter.ViewHolder>() {
    private lateinit var gifList: ArrayList<GiphyResponse.Data>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemGifBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_gif,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(gifList[position])
    }

    override fun getItemCount(): Int {
        return if (::gifList.isInitialized) gifList.size else 0
    }

    fun updateGifList(giphyResponse: GiphyResponse) {
        this.gifList = giphyResponse.data
        notifyDataSetChanged()
    }

    fun addToGifList(giphyResponse: GiphyResponse) {
        this.gifList.addAll(giphyResponse.data)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemGifBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = GifViewModel()
        fun bind(gif: GiphyResponse.Data) {
            viewModel.bind(gif)
            binding.viewModel = viewModel
        }
    }
}