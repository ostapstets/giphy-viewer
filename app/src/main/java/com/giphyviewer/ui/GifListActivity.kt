package com.giphyviewer.ui

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.giphyviewer.R
import com.giphyviewer.databinding.ActivityGifsBinding
import com.google.android.material.snackbar.Snackbar


class GifListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGifsBinding
    private lateinit var viewModel: GifListViewModel
    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gifs)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gifs)
        val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        binding.gifList.layoutManager = layoutManager

        viewModel = ViewModelProviders.of(this).get(GifListViewModel::class.java)
        binding.viewModel = viewModel

        viewModel = ViewModelProviders.of(this).get(GifListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer { errorMessage ->
            if (errorMessage != null) showError(errorMessage) else hideError()
        })
        binding.viewModel = viewModel
    }

    private fun showError(@StringRes errorMessage: Int) {
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError() {
        errorSnackbar?.dismiss()
    }
}
