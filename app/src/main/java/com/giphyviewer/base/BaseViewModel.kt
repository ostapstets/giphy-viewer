package com.giphyviewer.base

import androidx.lifecycle.ViewModel
import com.giphyviewer.injection.component.DaggerViewModelInjector
import com.giphyviewer.injection.component.ViewModelInjector
import com.giphyviewer.injection.module.NetworkModule
import com.giphyviewer.ui.GifListViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is GifListViewModel -> injector.inject(this)
        }
    }
}