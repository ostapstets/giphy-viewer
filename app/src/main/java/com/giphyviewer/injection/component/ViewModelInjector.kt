package com.giphyviewer.injection.component

import com.giphyviewer.injection.module.NetworkModule
import com.giphyviewer.ui.GifListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified GiphyViewModel.
     * @param giphyViewModel GiphyViewModel in which to inject the dependencies
     */
    fun inject(giphyViewModel: GifListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}