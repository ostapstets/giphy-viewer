package com.giphyviewer.model


import com.google.gson.annotations.SerializedName

data class GiphyResponse(
    val data: ArrayList<Data>,
    val pagination: Pagination?
) {
    data class Data(
        val images: Images?,
        val title: String?,
        val type: String?
    ) {
        data class Images(
            val downsized: Downsized?,
            @SerializedName("downsized_large")
            val downsizedLarge: DownsizedLarge?,
            @SerializedName("downsized_medium")
            val downsizedMedium: DownsizedMedium?,
            @SerializedName("fixed_height")
            val fixedHeight: FixedHeight?,
            @SerializedName("fixed_width")
            val fixedWidth: FixedWidth?,
            val original: Original?,
            @SerializedName("preview_gif")
            val previewGif: PreviewGif?,
            @SerializedName("preview_webp")
            val previewWebp: PreviewWebp?
        ) {
            data class Downsized(
                val height: String?,
                val size: String?,
                val url: String?,
                val width: String?
            )

            data class DownsizedLarge(
                val height: String?,
                val size: String?,
                val url: String?,
                val width: String?
            )

            data class DownsizedMedium(
                val height: String?,
                val size: String?,
                val url: String?,
                val width: String?
            )

            data class FixedHeight(
                val height: String?,
                val mp4: String?,
                @SerializedName("mp4_size")
                val mp4Size: String?,
                val size: String?,
                val url: String?,
                val webp: String?,
                @SerializedName("webp_size")
                val webpSize: String?,
                val width: String?
            )

            data class FixedWidth(
                val height: String?,
                val mp4: String?,
                @SerializedName("mp4_size")
                val mp4Size: String?,
                val size: String?,
                val url: String?,
                val webp: String?,
                @SerializedName("webp_size")
                val webpSize: String?,
                val width: String?
            )

            data class Original(
                val frames: String?,
                val height: String?,
                val mp4: String?,
                @SerializedName("mp4_size")
                val mp4Size: String?,
                val size: String?,
                val url: String?,
                val webp: String?,
                @SerializedName("webp_size")
                val webpSize: String?,
                val width: String?
            )

            data class PreviewGif(
                val height: String?,
                val size: String?,
                val url: String?,
                val width: String?
            )

            data class PreviewWebp(
                val height: String?,
                val size: String?,
                val url: String?,
                val width: String?
            )

        }
    }

    data class Pagination(
        val count: Int?,
        val offset: Int?,
        @SerializedName("total_count")
        val totalCount: Int?
    )
}