package com.giphyviewer.ui

import androidx.lifecycle.MutableLiveData
import com.giphyviewer.base.BaseViewModel
import com.giphyviewer.model.GiphyResponse

class GifViewModel : BaseViewModel() {
    private val gifUrl = MutableLiveData<String>()
    private val gifTitle = MutableLiveData<String>()
    private val gifRatio = MutableLiveData<String>()

    fun bind(data: GiphyResponse.Data) {
        val image = data.images?.fixedWidth
        gifUrl.value = image?.url
        gifTitle.value = data.title
        gifRatio.value = String.format("%s:%s", image?.width, image?.height)
    }

    fun getGifUrl(): MutableLiveData<String> {
        return gifUrl
    }

    fun getGifTitle(): MutableLiveData<String> {
        return gifTitle
    }

    fun getRatio(): MutableLiveData<String> {
        return gifRatio
    }
}