package com.giphyviewer.ui

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.giphyviewer.R
import com.giphyviewer.base.BaseViewModel
import com.giphyviewer.model.GiphyResponse
import com.giphyviewer.network.GiphyApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GifListViewModel : BaseViewModel() {

    @Inject
    lateinit var giphyApi: GiphyApi

    private val compositeDisposable = CompositeDisposable()
    val search: BehaviorSubject<String> = BehaviorSubject.createDefault("")
    val paginator: BehaviorSubject<Int> = BehaviorSubject.create<Int>()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val recyclerVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { search.onNext("") }
    val loadMoreListener: LoadMoreListener = LoadMoreListener()
    val gifListAdapter: GifListAdapter = GifListAdapter()

    private var itemCount = 25
    private var currentOffset = 0

    init {
        val disposableSearch = search.subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { onRetrieveGifListStart() }
            .doOnError { error -> onRetrieveGifListError(error) }
            .switchMap {
                if (it.isEmpty()) {
                    giphyApi.trending(itemCount, 0)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { error -> onRetrieveGifListError(error) }
                        .doOnTerminate { onRetrieveGifListFinish() }
                        .onErrorResumeNext(Observable.empty())
                } else {
                    giphyApi.search(it, itemCount, 0)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { error -> onRetrieveGifListError(error) }
                        .doOnTerminate { onRetrieveGifListFinish() }
                        .onErrorResumeNext(Observable.empty())
                }
            }

            .subscribe(
                { result -> onRetrieveGifSearchListSuccess(result) },
                { error -> onRetrieveGifListError(error) })

        compositeDisposable.add(disposableSearch as Disposable)

        val disposablePaginator = paginator.subscribeOn(Schedulers.io())
            .debounce(200, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .switchMap {
                if (search.value.isNullOrEmpty()) {
                    giphyApi.trending(itemCount, it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { error -> onRetrieveGifListError(error) }
                        .doOnTerminate {
                            loadMoreListener.paginationEnabled = true
                            onRetrieveGifListFinish()
                        }
                        .onErrorResumeNext(Observable.empty())
                } else {
                    giphyApi.search(search.value!!, itemCount, it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError { error -> onRetrieveGifListError(error) }
                        .doOnTerminate {
                            loadMoreListener.paginationEnabled = true
                            onRetrieveGifListFinish()
                        }
                        .onErrorResumeNext(Observable.empty())
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { loadMoreListener.paginationEnabled = true }
            .doOnError { error -> onRetrieveGifListError(error) }
            .subscribe(
                { result -> onRetrieveGifPaginationListSuccess(result) },
                { error -> onRetrieveGifListError(error) })
        compositeDisposable.add(disposablePaginator as Disposable)
    }

    private fun onRetrieveGifListStart() {
        loadingVisibility.value = View.VISIBLE
        recyclerVisibility.value = View.GONE
        errorMessage.value = null
    }

    private fun onRetrieveGifListFinish() {
        loadingVisibility.value = View.GONE
        recyclerVisibility.value = View.VISIBLE
    }

    private fun onRetrieveGifSearchListSuccess(giphyResponse: GiphyResponse) {
        gifListAdapter.updateGifList(giphyResponse)
        currentOffset = 0
    }

    private fun onRetrieveGifPaginationListSuccess(giphyResponse: GiphyResponse) {
        gifListAdapter.addToGifList(giphyResponse)
        currentOffset = giphyResponse.pagination?.offset!!
    }

    private fun onRetrieveGifListError(throwable: Throwable) {
        errorMessage.value = R.string.gif_error
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    inner class LoadMoreListener : RecyclerView.OnScrollListener() {
        private val visibleThreshold = 4
        var paginationEnabled = true

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if (paginationEnabled) {
                val layoutManager = recyclerView.layoutManager as StaggeredGridLayoutManager
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItems = layoutManager.findLastVisibleItemPositions(null)
                if (totalItemCount < itemCount) {
                    return
                }
                for (position in lastVisibleItems) {
                    if (totalItemCount <= position + visibleThreshold) {
                        paginationEnabled = false
                        paginator.onNext(currentOffset + itemCount)
                        continue
                    }
                }
            }
        }
    }
}
