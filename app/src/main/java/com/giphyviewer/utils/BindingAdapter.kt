package com.giphyviewer.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.giphyviewer.R
import com.giphyviewer.utils.extension.getParentActivity
import io.reactivex.subjects.BehaviorSubject


@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("scrollListener")
fun setAdapter(view: RecyclerView, listener: RecyclerView.OnScrollListener) {
    view.addOnScrollListener(listener)
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(
            parentActivity,
            Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}

@BindingAdapter("mutableSrc")
fun setImageUrl(view: ImageView, url: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && url != null) {
        url.observe(parentActivity, Observer { value ->
            GlideApp.with(view.context)
                .asGif()
                .placeholder(R.drawable.placeholder_image)
                .fitCenter()
                .load(value)
                .into(view)
        })
    }
}

@BindingAdapter("mutableRatio")
fun setRatio(view: ImageView, ratio: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && ratio != null) {
        ratio.observe(parentActivity, Observer { value ->
            run {
                val set = ConstraintSet()
                with(set) {
                    clone(view.parent as ConstraintLayout)
                    setDimensionRatio(view.id, value)
                    applyTo(view.parent as ConstraintLayout)
                }
            }
        }
        )

    }
}

@BindingAdapter("rxText")
fun rxText(editText: EditText, subject: BehaviorSubject<String?>) {
    editText.setText(subject.value)
    editText.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(
            charSequence: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            charSequence: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
        }

        override fun afterTextChanged(editable: Editable) {
            subject.onNext(editable.toString())
        }
    })
}

