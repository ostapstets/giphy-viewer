package com.giphyviewer.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


@GlideModule
class GiphyGlideApp : AppGlideModule()